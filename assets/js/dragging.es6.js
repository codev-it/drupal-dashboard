// noinspection DuplicatedCode,JSUnresolvedVariable

/**
 * @file
 * Behaviors of codev_dashboard module.
 */
(($, Drupal, GridStack) => {
  const { behaviors, ajax } = Drupal;

  Drupal.dashboard = {
    /**
     * Init handler.
     *
     * @type Boolean
     */
    _init: false,

    /**
     * GridStack wrapper element.
     *
     * @type Object
     */
    _grid: {},

    /**
     * Locked status.
     *
     * @type Boolean
     */
    _locked: true,

    /**
     * Lock jquery element.
     *
     * @type JQuery
     */
    _$locked: $('[data-dashboard-locked]'),

    /**
     * Lock data attribute name.
     *
     * @type String
     */
    _lockedDataAttrName: 'dashboard-locked',

    /**
     * Widget remove jquery element.
     *
     * @type JQuery
     */
    _$widgetRemove: $('[data-dashboard-widget-remove]'),

    /**
     * Init function.
     *
     * @param {HTMLDocument} context Current context element
     */
    init(context) {
      const $context = $(context);
      if ($context.find('.grid-stack').length && !this._init) {
        this._init = true;
        this._grid = GridStack.init();
        if (this._$locked.length) {
          this._locked = !!this._$locked.data(this._lockedDataAttrName);
          if (this._locked) {
            this.enableLocked();
          } else {
            this.disableLocked();
          }
        }

        this._$locked.on('click', () => {
          this.toggleLocked();
        });

        this._$widgetRemove.on('click', (event) => {
          const elem = event.target;
          const widget = elem.closest('.grid-stack-item');
          this._grid.removeWidget(widget);
          this.setCurrWidgetsPos();
        });

        this._grid.on('change', () => {
          this.setCurrWidgetsPos();
        });
      }
    },

    /**
     * Get the current grid object.
     *
     * @returns {Object} Return grid object
     */
    getGrid() {
      return this._grid;
    },

    /**
     * Check if grid is locked.
     *
     * @returns {Boolean} Return the locking status
     */
    isLocked() {
      return this._locked;
    },

    /**
     * Enable lock status.
     */
    enableLocked() {
      this._locked = true;
      this._$locked.attr(`data-${this._lockedDataAttrName}`, 1);
      this._grid.disable();
      this.update({ locked: this._locked });
    },

    /**
     * Disable lock status.
     */
    disableLocked() {
      this._locked = false;
      this._$locked.attr(`data-${this._lockedDataAttrName}`, 0);
      this._grid.enable();
      this.update({ locked: this._locked });
    },

    /**
     * Toggle locked status.
     */
    toggleLocked() {
      if (this._locked) {
        this.disableLocked();
      } else {
        this.enableLocked();
      }
    },

    /**
     * Get current widget data positions.
     *
     * @returns {Object} Widget position data
     */
    getCurrWidgetsPos() {
      const ret = {};
      const widgets = this._grid.el.querySelectorAll('.grid-stack-item') || [];
      widgets.forEach((widget) => {
        const id = widget.dataset.dashboardWidgetId;
        // noinspection JSUnresolvedVariable
        const node = widget.gridstackNode;
        ret[id] = {
          x: node.x,
          y: node.y,
          width: node.w,
          height: node.h
        };
      });
      return ret;
    },

    /**
     * Set current widget data positions.
     */
    setCurrWidgetsPos() {
      const widgets = this.getCurrWidgetsPos();
      this.update({ widgets: Object.keys(widgets).length ? widgets : '' });
    },

    /**
     * Update data function.
     *
     * @param {Object} data data to send
     */
    update(data) {
      this._ajax(data);
    },

    /**
     * Ajax request short handler
     *
     * @param {Object} data data to send
     * @param {String} url url path without uri
     *
     * @private
     */
    _ajax(data, url = 'dashboard') {
      ajax({ url, submit: data }).execute();
    }
  };

  /**
   * Dashboard dragging behaviors
   *
   * @type {{attach: (function(*=): void)}}
   */
  behaviors.codevDashboardDragging = {
    attach: (context) => Drupal.dashboard.init(context)
  };
})(jQuery, Drupal, GridStack);

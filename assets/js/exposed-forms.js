// noinspection All

/**
 * @file
 * Behaviors of codev_dashboard module.
 */
(function ($, Drupal) {
  var behaviors = Drupal.behaviors;
  /**
   * Ajax controller.
   *
   * @type {{attach: function}}
   */

  behaviors.codevDashboardExposedFormsReset = {
    attach: function attach(context) {
      $(document).ajaxSuccess(function () {
        var $viewFilters = $(context).find('.view-filters');

        if ($viewFilters.length) {
          var $resetButton = $viewFilters.find('.use-ajax-reset');
          $resetButton.once('dashboard-reset').on('click', function (event) {
            event.preventDefault();
            var $elem = $(event.target);
            var $form = $elem.parents('form');
            $form.find('.form-text').val('');
            $form.find('.form-select').val('All');
            $form.find('.trigger-submit').click();
          });
        }
      });
    }
  };
})(jQuery, Drupal);

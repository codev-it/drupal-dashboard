// noinspection DuplicatedCode

/**
 * @file
 * Behaviors of codev_dashboard module.
 */
(($, Drupal) => {
  const { behaviors } = Drupal;

  /**
   * Ajax controller.
   *
   * @type {{attach: function}}
   */
  behaviors.codevDashboardExposedFormsReset = {
    attach: (context) => {
      $(document).ajaxSuccess(() => {
        const $viewFilters = $(context).find('.view-filters');
        if ($viewFilters.length) {
          const $resetButton = $viewFilters.find('.use-ajax-reset');
          $resetButton.once('dashboard-reset').on('click', (event) => {
            event.preventDefault();
            const $elem = $(event.target);
            const $form = $elem.parents('form');
            $form.find('.form-text').val('');
            $form.find('.form-select').val('All');
            $form.find('.trigger-submit').click();
          });
        }
      });
    }
  };
})(jQuery, Drupal);

<?php

use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\views\ViewExecutable;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_dashboard.views.inc
 * .
 */

/**
 * Implements hook_preprocess_HOOK().
 *
 * @noinspection PhpUnused
 */
function codev_dashboard_preprocess_views_view(&$variables) {
  /** @var ViewExecutable $view */
  $view = $variables['view'];
  if (!empty($view)
    && $view->id() == 'content'
    && $view->current_display == 'dashboard') {
    $url = Url::fromUri('internal:/node/add');
    $markup = sprintf('<a href="%s" class="button button--action button--primary button--tiny margin-0">%s</a>',
      $url->toString(), t('Add content'));
    $variables['header'] = Markup::create($markup);
  }
}

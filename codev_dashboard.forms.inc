<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\Block;
use Drupal\views\ViewExecutable;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: codev_dashboard.forms.inc
 * .
 */

/**
 * Implements hook_form_BASE_FORM_ID_alter() for block_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_dashboard_form_block_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $storage = $form_state->getStorage();
  /** @var \Drupal\block\BlockForm $form_object */
  $form_object = $form_state->getFormObject();
  if (!empty($storage['block_theme'])
    && $storage['block_theme'] == 'codev_dashboard_theme') {
    unset($form['visibility'], $form['weight']);
    if ($form_object->getEntity()->isNew()) {
      $form['settings']['label_display']['#default_value'] = TRUE;
    }
    $form['#validate'] = ['_codev_dashboard_form_block_form_validate'];
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for views_exposed_form.
 *
 * @noinspection PhpUnused
 */
function codev_dashboard_form_views_exposed_form_alter(&$form, FormStateInterface $form_state) {
  $storage = $form_state->getStorage();
  if (!empty($storage['view']) && $storage['view'] instanceof ViewExecutable) {
    $view = $storage['view'];
    if ($view->id() == 'content') {
      /** @var Block $display */
      $display = $view->getDisplay();
      if ($display instanceof Block && $display->display['id'] == 'dashboard') {
        $form['#attached']['library'][] = 'codev_dashboard/exposed-forms';
        if (!empty($form['actions']['reset'])) {
          $form['actions']['reset']['#attributes']['class'][] = 'use-ajax-reset';
          $form['actions']['submit']['#attributes']['class'][] = 'trigger-submit';
        }
      }
    }
  }
}

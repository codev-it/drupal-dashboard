<?php
/**
 * @file
 * Contains Drupal\welcome\Form\MessagesForm.
 */

namespace Drupal\codev_dashboard\Form;

use Drupal\codev_dashboard\BlockManager;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Exception;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: AdminSettingsForm.php
 * .
 */

/**
 * Class AdminSettingsForm.
 *
 * @package      Drupal\codev_dashboard\Form
 *
 * @noinspection PhpUnused
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpUnused
   */
  public function getFormId(): string {
    return 'codev_dashboard_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'codev_dashboard.settings',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @param array                                $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form state object.
   *
   * @return array
   *
   * @noinspection PhpUnused
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config($this->getEditableConfigNames()[0]);
    $data = $config->getRawData();

    $form['config_type'] = [
      '#title'         => $this->t('Select dashboard configuration type:'),
      '#type'          => 'select',
      '#options'       => [
        'global' => $this->t('Global'),
        'roles'  => $this->t('User roles'),
      ],
      '#default_value' => Utils::getArrayValue('config_type', $data, []),
      '#description'   => $this->t('Selecting a configuration type determines the type of block management within the dashboard.'),
      '#required'      => TRUE,
    ];

    $form['locked'] = [
      '#title'         => $this->t('Lock by default:'),
      '#type'          => 'select',
      '#options'       => [
        FALSE => t('No'),
        TRUE  => t('Yes'),
      ],
      '#default_value' => Utils::getArrayValue('locked', $data, TRUE),
      '#description'   => $this->t('Select if the dashboard by default locked.'),
      '#required'      => TRUE,
    ];

    $url = Url::fromUri('internal:/admin/structure/block/list/codev_dashboard_theme');
    $text = t('You can configure the available blocks in the <a href="@url">Block layout</a> area of the Dashboard Theme tab.', [
      '@url' => $url->toString(),
    ]);
    $markup = sprintf('<p>%s</p>', $text);
    $form['block_settings'] = [
      '#markup' => $markup,
    ];

    $form['defaults_cn'] = [
      '#type'       => 'container',
      '#attributes' => ['id' => 'defaults-wrapper'],
    ];

    $markup = sprintf('<p><strong>%s</strong></p>',
      $this->t('Default dashboard blocks:'));
    $form['defaults_cn']['title'] = ['#markup' => $markup];

    if ($form_state->isRebuilding()) {
      $form['defaults_cn']['unsaved'] = [
        '#markup'     => sprintf(
          '<div class="messages messages--warning">%s</div>',
          $this->t('You have unsaved changes.')),
        '#attributes' => [
          'class' => [
            'view-changed',
            'messages',
            'messages--warning',
          ],
        ],
      ];
    }

    $form['defaults_cn']['defaults'] = [
      '#type'   => 'table',
      '#header' => [
        $this->t('Block'),
        $this->t('Settings'),
      ],
      '#empty'  => $this->t('Sorry, There are no blocks!'),
      '#prefix' => '<div id="defaults-wrapper">',
      '#suffix' => '</div>',
    ];

    $defaults = Utils::getArrayValue('defaults', $data, []);
    $default_key = array_keys($defaults);
    $default_blocks_selected = $form_state->get('default_blocks_selected')
      ?: array_combine($default_key, $default_key);
    foreach ($defaults as $key => $data) {
      $form['defaults_cn']['defaults'][$key] = $this->tableItem($key, $data, $default_blocks_selected);
    }

    for ($i = 0; $i < $form_state->get('default_blocks_append') ?: 0; $i++) {
      $form['defaults_cn']['defaults'][$i] = $this->tableItem($i, [], $default_blocks_selected);
    }

    $default_blocks_unset = $form_state->get('default_blocks_unset') ?: [];
    if (!empty($default_blocks_unset)) {
      foreach ($default_blocks_unset as $item) {
        if (!empty($form['defaults_cn']['defaults'][$item])) {
          unset($form['defaults_cn']['defaults'][$item]);
        }
      }
    }

    if (count($default_blocks_selected) !== count($this->buildDefaultBlockOptions(NULL, []))) {
      $form['defaults_cn']['defaults']['add_new_button']['cn'] = [
        '#type'               => 'container',
        '#wrapper_attributes' => [
          'colspan' => 2,
        ],
        'button'              => [
          '#type'   => 'submit',
          '#value'  => $this->t('Add new'),
          '#submit' => ['::addNewDefaultItemElement'],
          '#ajax'   => [
            'callback' => '::updateDefaultsElementTable',
            'wrapper'  => 'defaults-wrapper',
          ],
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element['#submit'])
      && in_array('::deleteDefaultItemElement', $triggering_element['#submit'])) {
      $form_state->clearErrors();
    }
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpUnused
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config($this->getEditableConfigNames()[0]);
    $config->set('config_type', $form_state->getValue('config_type'))
      ->set('locked', $form_state->getValue('locked'));
    if (!empty($form_state->getValue('defaults'))) {
      $defaults = [];
      foreach ($form_state->getValue('defaults') ?: [] as $item) {
        if (!empty($item['select']) && !empty($item['settings'])) {
          $key = $item['select'];
          if (!empty($item['settings']['remove'])) {
            unset($item['settings']['remove']);
          }
          $defaults[$key] = $item['settings'];
        }
      }
      if (!empty($defaults)) {
        $config->set('defaults', $defaults);
      }
    }
    $config->save();
  }

  /**
   * Table items preset.
   *
   * @param string|int $key
   *   Table key.
   * @param array      $data
   *   Table data.
   * @param array      $selected_options
   *   Additional options.
   *
   * @return array
   */
  protected function tableItem($key, array $data, array $selected_options): array {
    $item = [];
    $selected = Utils::getArrayValue($key, $selected_options, '');
    if (!empty($selected) && in_array($key, $selected_options)) {
      unset($selected_options[$key]);
    }
    $item['select'] = [
      '#type'          => 'select',
      '#options'       => $this->buildDefaultBlockOptions($selected, array_values($selected_options)),
      '#default_value' => $key ?: '',
      '#required'      => TRUE,
    ];

    $item['settings'] = [
      '#type'       => 'container',
      '#attributes' => [
        'class' => [
          'container-inline',
        ],
      ],
    ];
    $item['settings']['x'] = [
      '#type'          => 'textfield',
      '#title'         => t('X'),
      '#default_value' => Utils::getArrayValue('x', $data, '0'),
      '#size'          => 4,
      '#required'      => TRUE,
    ];
    $item['settings']['y'] = [
      '#type'          => 'textfield',
      '#title'         => t('Y'),
      '#default_value' => Utils::getArrayValue('y', $data, '0'),
      '#size'          => 4,
      '#required'      => TRUE,
    ];
    $item['settings']['width'] = [
      '#type'          => 'textfield',
      '#title'         => t('Width'),
      '#default_value' => Utils::getArrayValue('width', $data, '0'),
      '#size'          => 4,
      '#required'      => TRUE,
    ];
    $item['settings']['height'] = [
      '#type'          => 'textfield',
      '#title'         => t('Height'),
      '#default_value' => Utils::getArrayValue('height', $data, '0'),
      '#size'          => 4,
      '#required'      => TRUE,
    ];
    $item['settings']['remove'] = [
      '#type'       => 'submit',
      '#value'      => $this->t('Remove'),
      '#name'       => $key,
      '#submit'     => ['::deleteDefaultItemElement'],
      '#ajax'       => [
        'callback' => '::updateDefaultsElementTable',
        'wrapper'  => 'defaults-wrapper',
      ],
      '#attributes' => [
        'class' => [
          'button',
          'button--danger',
        ],
      ],
    ];
    return $item;
  }

  /**
   * Build the current block to select and exclude existed chooses.
   *
   * @param string|int|null $id
   * @param array           $selected
   *
   * @return array
   */
  private function buildDefaultBlockOptions($id = NULL, array $selected = []): array {
    try {
      /** @var \Drupal\block\Entity\Block[] $blocks */
      $blocks = BlockManager::getList();
      $ret = [];
      foreach ($blocks as $block_id => $block) {
        if ($block_id == $id || !in_array($block_id, $selected)) {
          $ret[$block_id] = $block->label();
        }
      }
      return $ret;
    } catch (Exception $exception) {
      return [];
    }
  }

  /**
   * Build the selected block array.
   *
   * @param array $arr
   *
   * @return array
   */
  private function buildSelectedBlocks(array $arr = []): array {
    $ret = [];
    foreach ($arr as $key => $val) {
      if (!empty($val['select'])) {
        $ret[$key] = $val['select'];
      }
    }
    $ret[] = array_keys($this->buildDefaultBlockOptions(NULL, $ret))[0];
    return $ret;
  }

  /**
   * Update current defaults element callback
   *
   * @param array                                $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form state object.
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function updateDefaultsElementTable(array $form, FormStateInterface $form_state) {
    return $form['defaults_cn'];
  }

  /**
   * Update callback for ajax
   *
   * @param array                                $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form state object.
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function addNewDefaultItemElement(array &$form, FormStateInterface $form_state) {
    $default_blocks_append = $form_state->get('default_blocks_append') ?: 0;
    $form_state->set('default_blocks_append', $default_blocks_append + 1);
    $default_values = $form_state->getValue('defaults') ?: [];
    $default_blocks_selected = $this->buildSelectedBlocks($default_values);
    $form_state->set('default_blocks_selected', $default_blocks_selected);
    $form_state->setRebuild();
  }

  /**
   * Remove callback for ajax
   *
   * @param array                                $form
   *   Drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form state object.
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function deleteDefaultItemElement(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $block_unset_name = $triggering_element['#name'];
    $default_blocks_unset = $form_state->get('default_blocks_unset') ?: [];
    if (!in_array($block_unset_name, $default_blocks_unset)) {
      $default_blocks_unset[] = $block_unset_name;
    }
    $form_state->set('default_blocks_unset', $default_blocks_unset);
    $form_state->setRebuild();
  }

}

<?php

namespace Drupal\codev_dashboard;

use Drupal\codev_utils\SettingsBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\Role;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Settings.php
 * .
 */

/**
 * Class Settings.
 *
 * @package      Drupal\codev_dashboard
 *
 * @noinspection PhpUnused
 */
class Settings extends SettingsBase {

  /**
   * {@inheritDoc}
   *
   * @noinspection PhpUnused
   */
  const MODULE_NAME = 'codev_dashboard';

  /**
   * Get the access key from the user
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User account object.
   *
   * @return array
   */
  public static function getAccessKeys(AccountInterface $user): array {
    $config_type = static::get('config_type');
    if (!empty($config_type)) {
      switch ($config_type) {
        case 'global':
          return ['global'];
        case 'roles':
          $ret = [];
          $roles = Role::loadMultiple();
          $user_roles = $user->getRoles();
          foreach ($roles as $role) {
            $id = intval($role->getWeight());
            $ret[$id] = $role->id();
          }
          ksort($ret);
          $count = 0;
          foreach (array_values($ret) as $key => $val) {
            if (in_array($val, $user_roles)) {
              $count = $key + 1;
            }
          }
          return array_slice($ret, 0, $count);
      }
    }
    return [];
  }

  /**
   * Get theme regions array
   *
   * @return array
   */
  public static function getThemeRegions(): array {
    $ret = [];
    $type_infos = static::getConfigTypeInfos();
    foreach ($type_infos as $key => $type_info) {
      if (is_array($type_info) && !empty($type_info['label'])) {
        $ret[$key] = $type_info['label'];
      }
      else {
        if (is_object($type_info)) {
          $ret[$key] = $type_info->label();
        }
        else {
          $ret[$key] = $key;
        }
      }
    }
    return $ret;
  }

  /**
   * Return config type info
   *
   * @return array|\Drupal\Core\Entity\EntityInterface[]|\Drupal\user\Entity\Role[]
   */
  public static function getConfigTypeInfos(): array {
    $config_type = static::get('config_type');
    if (!empty($config_type)) {
      switch ($config_type) {
        case 'global':
          return ['global' => ['label' => 'Global']];
        case 'roles':
          return Role::loadMultiple();
      }
    }
    return [];
  }

  /**
   * Set widget postions for default widgets.
   *
   * @param array $data
   *
   * @return void
   */
  public static function setDefaultWidgets(array $data) {
    $defaults = Settings::get('defaults');
    foreach ($data as $id => $item) {
      if (!isset($item['x'])) {
        $item['x'] = -1;
      }
      if (!isset($item['y'])) {
        $item['y'] = -1;
      }
      if (!empty($item['width']) && !empty($item['height'])) {
        $defaults[$id] = $item;
      }
    }
    Settings::set('defaults', $defaults);
  }

}

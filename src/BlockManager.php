<?php
/**
 * Created by PhpStorm.
 * User: Coser Angelo
 * Date: 20.09.19
 * Time: 22:37
 */

namespace Drupal\codev_dashboard;


use Drupal;
use Drupal\Core\Session\AccountInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: BlockManager.php
 * .
 */

/**
 * Class BlockManager.
 *
 * @package Drupal\codev_dashboard
 */
class BlockManager {

  /**
   * Get all existing blocks
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getList(): array {
    return Drupal::entityTypeManager()
      ->getStorage('block')
      ->loadByProperties([
        'theme' => 'codev_dashboard_theme',
      ]);
  }

  /**
   * Get all existing blocks for given user
   *
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User account object.
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getListByUser(AccountInterface $user): array {
    $ret = [];
    $access_keys = Settings::getAccessKeys($user);
    /** @var \Drupal\block\Entity\Block $item */
    foreach (static::getList() as $key => $item) {
      if (in_array($item->getRegion(), $access_keys)) {
        $ret[$key] = $item;
      }
    }
    return $ret;
  }

}

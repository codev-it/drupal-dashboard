<?php

namespace Drupal\codev_dashboard\Controller;

use Drupal\codev_dashboard\Settings;
use Drupal\codev_utils\Helper\Block;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserData;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: DashboardController.php
 * .
 */

/**
 * Class DashboardController.
 *
 * @package      Drupal\codev_dashboard\Controller
 *
 * @noinspection PhpUnused
 */
class DashboardController extends ControllerBase {

  /**
   * User data
   *
   * @var UserData
   */
  protected UserData $userData;

  /**
   * Database
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * DashboardController constructor.
   *
   * @param \Drupal\user\UserData             $user_data
   *   User data object.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   Account proxy object.
   */
  function __construct(AccountProxyInterface $current_user, UserData $user_data, Connection $database) {
    $this->currentUser = $current_user;
    $this->userData = $user_data;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ControllerBase {
    /** @noinspection PhpParamsInspection */
    return new static(
      $container->get('current_user'),
      $container->get('user.data'),
      $container->get('database')
    );
  }

  /**
   * Dashboard
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Http request object.
   *
   * @return array|\Drupal\Core\Ajax\AjaxResponse
   */
  public function render(Request $request) {
    if (!$request->isXmlHttpRequest()) {
      $widgets = $this->getAddedWidgets();
      $build = $this->buildItems($widgets);
      return [
        '#theme'      => 'dashboard',
        '#items'      => Utils::getArrayValue('items', $build, []),
        '#items_attr' => Utils::getArrayValue('attr', $build, []),
        '#locked'     => $this->getLocked(),
        '#attached'   => [
          'library' => [
            'codev_dashboard/dragging',
          ],
        ],
      ];
    }
    else {
      if ($request->request->get('_drupal_ajax')) {
        if ($this->checkRequestKeyExist('widgets', $request)) {
          $widgets = $request->request->get('widgets');
          $this->setUserWidgets($widgets ?: []);
        }

        if ($this->checkRequestKeyExist('locked', $request)) {
          $locked = $request->request->get('locked');
          $this->setLocked($locked === 'true');
        }
      }
      return new AjaxResponse();
    }
  }

  /**
   * Check if request key exist
   *
   * @param                                           $key
   *   Request key.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Http request object.
   *
   * @return bool
   */
  function checkRequestKeyExist($key, Request $request): bool {
    $items = array_keys($request->request->getIterator()->getArrayCopy());
    return in_array($key, $items);
  }

  /**
   * Build render items
   *
   * @param $widgets array
   *                 Widgets array.
   *
   * @return array
   */
  private function buildItems(array $widgets): array {
    $ret = [];
    $locked = $this->getLocked();
    foreach ($widgets as $id => $widget) {
      /** @var \Drupal\block\Entity\Block $block */
      if ($block = Block::loadRenderable($id) ?: NULL) {
        $ret['items'][$id] = $block;
        $ret['attr'][$id] = $this->createDataAttrs($widget);
        $ret['attr'][$id]['data-dashboard-widget-id'] = $id;
        $ret['attr'][$id]['gs-locked'] = $locked;
        $ret['attr'][$id]['gs-noMove'] = $locked;
        $ret['attr'][$id]['gs-noResize'] = $locked;
      }
    }
    return $ret;
  }

  /**
   * Create item attributes
   *
   * @param $info
   *   Dashboard info array.
   *
   * @return array
   */
  protected function createDataAttrs($info): array {
    if ((isset($info['x']) && $info['x'] != -1)
      || (isset($info['y']) && $info['y'] != -1)) {
      return [
        'gs-x' => Utils::getArrayValue('x', $info, 0),
        'gs-y' => Utils::getArrayValue('y', $info, 0),
        'gs-w' => Utils::getArrayValue('width', $info, 0),
        'gs-h' => Utils::getArrayValue('height', $info, 0),
      ];
    }
    else {
      return [
        'gs-w'            => Utils::getArrayValue('width', $info, 0),
        'gs-h'            => Utils::getArrayValue('height', $info, 0),
        'gs-autoPosition' => TRUE,
      ];
    }
  }

  /**
   * Get current widget they are added by the user
   *
   * @return array
   */
  protected function getAddedWidgets(): array {
    if (!$this->checkIfWidgetUserDataExist()) {
      $def = $this->defWidgets();
      $this->setUserWidgets($def);
      $this->setLocked(Settings::get('locked'));
      return $def;
    }
    else {
      return $this->getUserWidgets();
    }
  }

  /**
   * Check if user has existed widget data
   *
   * @return bool
   */
  private function checkIfWidgetUserDataExist(): bool {
    $user = $this->currentUser;
    $query = $this->database->select('users_data', 'u');
    $query->fields('u', []);
    $query->condition('uid', $user->id());
    $query->condition('name', 'widgets');
    return !empty($query->execute()->fetchAll());
  }

  /**
   * Default widget definitions
   *
   * @return array
   */
  protected function defWidgets(): array {
    return Settings::get('defaults');
  }

  /**
   * Set widgets to user
   *
   * @param array $widgets
   *   Widgets array.
   */
  protected function setUserWidgets(array $widgets) {
    if ($this->checkSaveData($widgets)) {
      $this->userData
        ->set('codev_dashboard', $this->currentUser->id(), 'widgets', $widgets);
    }
  }

  /**
   * Get widgets from user
   *
   * @return array
   */
  protected function getUserWidgets(): array {
    $uid = $this->currentUser->id();
    return $this->userData->get('codev_dashboard', $uid, 'widgets') ?: [];
  }

  /**
   * Check data before save
   *
   * @param array $data
   *   Dashboard data to store.
   *
   * @return bool
   */
  private function checkSaveData(array $data): bool {
    $accepted = ['x', 'y', 'width', 'height'];
    foreach ($data as $id => $item) {
      $id = strtr($id, ['-', '_']);
      if (preg_match('/\W/m', $id) == 0) {
        foreach ($item as $key => $val) {
          if (!in_array($key, $accepted)) {
            return FALSE;
          }
          if (preg_match('/\D/m', $val) == 1) {
            return FALSE;
          }
        }
      }
    }
    return TRUE;
  }

  /**
   * Set dashboard lock
   *
   * @param bool $val
   *   Lock status to store.
   */
  private function setLocked(bool $val) {
    $this->userData
      ->set('codev_dashboard', $this->currentUser->id(), 'locked', $val);
  }

  /**
   * Get dashboard lock
   *
   * @return bool
   */
  private function getLocked(): bool {
    return $this->userData
      ->get('codev_dashboard', $this->currentUser->id(), 'locked');
  }

}

<?php

namespace Drupal\codev_dashboard\Controller;

use Drupal\codev_dashboard\BlockManager;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: WidgetManagerController.php
 * .
 */

/**
 * Class WidgetManagerController.
 *
 * @package      Drupal\codev_dashboard\Controller
 *
 * @noinspection PhpUnused
 */
class WidgetManagerController extends DashboardController {

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Block view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected EntityViewBuilderInterface $blockViewBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ControllerBase {
    $instance = parent::create($container);
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $container->get('entity_type.manager');
    $instance->messenger = $container->get('messenger');
    $instance->blockViewBuilder = $entityTypeManager->getViewBuilder('block');
    return $instance;
  }

  /**
   * Widget list page
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Http request object.
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @noinspection PhpUnused
   */
  public function render(Request $request): array {
    $items = [];
    $items_attr = [];
    /** @var \Drupal\Core\Session\AccountProxy $curr_user */
    $curr_user = $this->currentUser;
    $blocks = BlockManager::getListByUser($curr_user->getAccount());
    $placed_blocks = array_keys($this->getAddedWidgets());
    /** @var \Drupal\block\Entity\Block $block */
    foreach ($blocks as $id => $block) {
      if (!in_array($id, $placed_blocks)) {
        $items[$id] = $this->blockViewBuilder->view($block);
        $items_attr[$id]['data-dashboard-widget-id'] = $id;
      }
    }
    return [
      '#theme'      => 'dashboard__widgets',
      '#items'      => $items,
      '#items_attr' => $items_attr,
      '#attached'   => [
        'library' => [
          'codev_dashboard/main',
        ],
      ],
    ];
  }

  /**
   * Widget add item page
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Http request object.
   * @param string                                    $widget
   *   Widget id.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *
   * @noinspection PhpUnused
   * @noinspection PhpUnusedParameterInspection
   */
  public function addWidget(Request $request, string $widget): RedirectResponse {
    $current_widgets = $this->getUserWidgets();
    if (!empty($widget) && empty($current_widgets[$widget])) {
      $current_widgets[$widget] = $this->appendToBottom(6, 4, $current_widgets);
      $this->setUserWidgets($current_widgets);
      $this->messenger->addStatus(t('Widget added to Dashboard.'));
    }
    elseif (!empty($current_widgets[$widget])) {
      $this->messenger->addError(t('Widget already exists in Dashboard.'));
    }
    $url = Url::fromRoute('codev_dashboard.dashboard');
    return new RedirectResponse($url->toString());
  }

  /**
   * Append new item on the bottom of the list
   *
   * @param int   $width
   *   Width to cal.
   * @param int   $height
   *   Height to cal.
   * @param array $widgets
   *   Widgets array.
   *
   * @return array
   */
  function appendToBottom(int $width, int $height, array $widgets): array {
    $max_y_items = [];
    $max_y_count = 0;
    $max_x_count = 0;
    foreach ($widgets as $widget) {
      $max_y = $widget['y'] + $widget['height'];
      if ($max_y >= $max_y_count) {
        if ($max_y > $max_y_count) {
          $max_y_items = [];
        }
        $max_y_items[] = $widget;
        $max_y_count = $max_y;
      }
    }
    if (count($max_y_items) == 1) {
      $max_y_count = $max_y_count - $max_y_items[0]['height'];
      if ($max_y_items[0]['x'] < 5) {
        $max_x_count = 6;
      }
    }
    return [
      'x'      => $max_x_count,
      'y'      => $max_y_count,
      'width'  => $width,
      'height' => $height,
    ];
  }

}

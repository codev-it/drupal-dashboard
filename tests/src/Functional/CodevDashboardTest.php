<?php

namespace Drupal\Tests\codev_dashboard\Functional;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: CodevDashboardTest.php
 * .
 */

/**
 * Class CodevDashboardTest.
 *
 * Tests installation module expectations.
 *
 * @package      Drupal\Tests\codev_dashboard\Functional
 *
 * @group        codev_dashboard
 *
 * @noinspection PhpUnused
 */
class CodevDashboardTest extends FunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_dashboard',
  ];

  /**
   * The admin user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp() {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([
      'use dashboard',
      'use widgets',
      'administer blocks',
      'administer dashboard',
      'administer themes codev_dashboard_theme',
    ]);
    /** @var \Drupal\Core\Config\Config $config */
    $this->drupalPlaceBlock('system_main_block', ['theme' => 'codev_dashboard_theme']);
    $this->drupalPlaceBlock('user_login_block', ['theme' => 'codev_dashboard_theme']);
  }

  /**
   * Tests the functionality.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testCodevDashboard() {
    // Check admin form and field exist.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/system/codev_dashboard');
    $this->assertSession()
      ->pageTextContains(t('Select dashboard configuration type:'));
    $this->assertSession()->pageTextContains(t('Lock by default:'));
    $this->assertSession()->pageTextContains(t('Default dashboard blocks:'));
    $this->assertSession()->pageTextContains(t('Block'));
    $this->assertSession()->pageTextContains(t('Settings'));
    $this->assertSession()->buttonExists(t('Add new'));
    $this->assertSession()->buttonExists(t('Remove'));
    $this->assertSession()->statusCodeEquals(200);

    // dashboard theme
    $this->drupalGet('admin/structure/block/list/codev_dashboard_theme');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(t('Global'));

    // dashboard overview
    $this->drupalGet('dashboard');
    $this->assertSession()->linkExists(t('Add new widget'));
    $this->assertSession()->pageTextContains(t('Lock widget'));
    $this->assertSession()->statusCodeEquals(200);

    // widgets
    $this->drupalGet('dashboard/widgets');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('dashboard/widgets/add/system_main_block');
    $this->assertSession()->statusCodeEquals(200);

    // anonymous tests
    $this->drupalLogout();
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('admin/config/system/codev_dashboard');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('dashboard');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('dashboard/widgets/add/system_main_block');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet('dashboard/widgets');
    $this->assertSession()->statusCodeEquals(403);
  }

}

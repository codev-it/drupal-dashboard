<?php

namespace Drupal\Tests\codev_dashboard\Kernel;

use Drupal\block\Entity\Block;
use Drupal\codev_dashboard\BlockManager;
use Drupal\KernelTests\KernelTestBase;
use Drupal\user\Entity\User;

/**
 * Unit tests for the BlockManager utility class.
 *
 * @group        codev_dashboard
 *
 * @noinspection PhpUnused
 */
class BlockManagerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_dashboard',
    'system',
    'block',
    'user',
  ];

  /**
   * {@inheritDoc}
   *
   * @throws \Exception
   */
  protected function setUp() {
    parent::setUp();
    $this->installSchema('system', ['sequences']);
    $this->installSchema('user', ['users_data']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('block');
    $this->installConfig(['codev_dashboard', 'system']);

    /** @var \Drupal\Core\Extension\ThemeInstaller $theme_installer */
    $theme_installer = $this->container->get('theme_installer');
    $theme_installer->install(['codev_dashboard_theme']);
  }

  /**
   * Test: BlockManager::getList().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testGetList() {
    $this->assertEmpty(BlockManager::getList());
    Block::create([
      'id'     => 'test',
      'plugin' => 'page_title_block',
      'theme'  => 'codev_dashboard_theme',
    ])->save();
    $blocks_after = BlockManager::getList();
    $this->assertTrue(in_array('test', array_keys($blocks_after)));
  }

  /**
   * Test: BlockManager::getListByUser
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testGetListByUser() {
    /** @var Block $block */
    $block = Block::create([
      'id'     => 'test',
      'plugin' => 'page_title_block',
      'theme'  => 'codev_dashboard_theme',
    ]);
    $block->setRegion('global');
    $block->save();

    // No access without permissions.
    $user = User::create(['id' => 1, 'name' => 'test']);
    $user->save();
    $this->assertEquals(['test'],
      array_keys(BlockManager::getListByUser($user)));
  }

}

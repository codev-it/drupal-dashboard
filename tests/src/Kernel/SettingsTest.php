<?php

namespace Drupal\Tests\codev_dashboard\Kernel;

use Drupal\codev_dashboard\Settings;
use Drupal\Core\Session\UserSession;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Unit tests for the ConfigSettings utility class.
 *
 * @group        codev_dashboard
 *
 * @noinspection PhpUnused
 */
class SettingsTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'codev_dashboard',
    'user',
  ];

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  public $configFactory;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   *
   * @noinspection PhpUnused
   */
  public function setUp() {
    parent::setUp();
    $this->installConfig(['codev_dashboard']);
    $this->configFactory = $this->container->get('config.factory');
  }

  /**
   * Test: Settings::getConfigTypeInfos
   */
  public function testGetConfigTypeInfo() {
    $infos = Settings::getConfigTypeInfos();
    $this->assertEquals(['global' => ['label' => 'Global']], $infos);
  }

  /**
   * Test: Settings::getThemeRegions
   */
  public function testGetThemeRegions() {
    $regions = Settings::getThemeRegions();
    $this->assertEquals(['global' => 'Global'], $regions);
  }

  /**
   * Test: Settings::getAccessKeys
   */
  public function testGetAccessKeys() {
    // global settings
    $global = Settings::getAccessKeys(new UserSession());
    $this->assertEquals(['global'], $global);

    // roles settings
    $config = $this->configFactory->getEditable('codev_dashboard.settings');
    $config->set('config_type', 'roles')->save();
    $this->createRole([], 'anonymous', 'anonymous', 0);
    $this->createRole([], 'authenticated', 'authenticated', 1);

    $anonymous = Settings::getAccessKeys(new UserSession([
      'roles' => ['anonymous'],
    ]));
    $this->assertEquals(['anonymous'], $anonymous);

    $anonymous = Settings::getAccessKeys(new UserSession([
      'roles' => ['authenticated'],
    ]));
    $this->assertEquals(['anonymous', 'authenticated'], $anonymous);
  }

  /**
   * Test: Settings::setDefaultWidgets
   */
  public function testSetDefaultWidgets() {
    $this->assertWidgetConfig('test_auto', [
      'width'  => 6,
      'height' => 4,
    ], [
      'width'  => 6,
      'height' => 4,
      'x'      => -1,
      'y'      => -1,
    ]);


    $position = ['x' => 0, 'y' => 0, 'width' => 6, 'height' => 4];
    $this->assertWidgetConfig('test_1', $position, $position);

    $position = ['x' => 1, 'y' => 1, 'width' => 6, 'height' => 4];
    $this->assertWidgetConfig('test_2', $position, $position);
  }

  /**
   * Test widget postion config.
   *
   * @param string $id
   * @param array  $current
   * @param array  $accept
   *
   * @return void
   */
  private function assertWidgetConfig(string $id, array $current, array $accept) {
    Settings::setDefaultWidgets([$id => $current]);

    $data = $this->configFactory->get('codev_dashboard.settings')->getRawData();
    $this->assertArrayHasKey('defaults', $data);
    $this->assertArrayHasKey($id, $data['defaults']);
    $this->assertEquals($accept, $data['defaults'][$id]);
  }

}
